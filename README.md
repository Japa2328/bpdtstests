Test city_success validates API 200 response when path paramter for city not empty

Test empty_city_error validates API 404 response when path parameter for city is empty

Test Instructions validates body message returned from API

Test get_user_info validates API message data returned for all 1000 user ids

Test invalid_user validates error response from API when id does not exist

Test Users validates body message returned from API matches the expected response.json file